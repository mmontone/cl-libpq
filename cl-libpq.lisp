(in-package :libpq)

(defclass pg-connection ()
  ((conn-ptr :initarg :conn-ptr
			 :initform (error "Provide the connection pointer")
			 :accessor conn-ptr)
   (host :initarg host
		 :initform "localhost"
		 :accessor pg-connection-host)
   (port :initarg port
		 :initform 5432
		 :accessor pg-connection-port)
   (dbname :initarg :dbname
		   :initform ""
		   :accessor pg-connection-dbname)
   (lock :accessor database-lock
		 :initform (bt:make-lock "conn"))))

(export 'pg-connection)

(defmethod print-object ((pg-connection pg-connection) stream)
  (print-unreadable-object (pg-connection stream :type t :identity t)
	(with-slots (host port dbname) pg-connection
	  (format stream "~A:~A ~A"
			  host port dbname))))

(defvar *pg-connection*)

(defun pg-connect-toplevel (&rest args)
  (setf *pg-connection* (apply #'pg-connect args)))

(export 'pg-connect-toplevel)

(defun call-with-pg-connection (pg-connection function)
  (let ((*pg-connection* pg-connection))
	(funcall function)))

(defun build-connection-string (args)
  (with-output-to-string (s)
	(loop
	   :for arg :in args :by #'cddr
	   :for value :in (cdr args) :by #'cddr
	   :do
	   (format s "~A=~A " (string-downcase arg) value))))		 

(defun pg-connect (&rest args &key host user password dbname port)
  (let ((conn-ptr
		 (libpq-ffi:PQconnectdb (build-connection-string args))))
	(ecase (libpq-ffi:pqstatus conn-ptr)
	  (:connection_ok (apply #'make-instance 'pg-connection
							 (list* :allow-other-keys t
									:conn-ptr conn-ptr
									args)))
	  (:connection_bad
	   (let ((error-msg (libpq-ffi:PQerrormessage conn-ptr)))
		 (libpq-ffi:PQfinish conn-ptr)
		 (error error-msg))))))

(export 'pg-connect)

(defun pg-disconnect (&optional (pg-connection *pg-connection*))
  (libpq-ffi:PQfinish (conn-ptr pg-connection))
  (setf (conn-ptr pg-connection) nil)
  t)

(export 'pg-disconnect)

(defun pgsql-ftype-value (key)
  (cffi:foreign-enum-value 'libpq-ffi::pgsql-ftype key))

(defun make-type-list-for-auto (num-fields res-ptr)
  (let ((new-types '()))
	(dotimes (i num-fields)
	  (declare (fixnum i))
	  (let* ((type (libpq-ffi:PQftype res-ptr i)))
		(push
		 (cond
		   ((member type (mapcar #'pgsql-ftype-value
								 (list :int2
									   :int4)))
			:int32)
		   ((member type (list (pgsql-ftype-value :int8)))
			:int64)
           ((member type (mapcar #'pgsql-ftype-value (list :float4 :float8)))
            :double)
           (t :string))
         new-types)))
	(nreverse new-types)))

(defun result-field-names (num-fields res-ptr)
  (let ((field-names '()))
	(dotimes (i  num-fields)
	  (declare (fixnum i))
	  (let ((field-name (libpq-ffi:PQfname res-ptr i)))
		(push field-name field-names)))
	(nreverse field-names)))

(defun strtoul (char-ptr)
  (libpq-ffi::c-strtoul char-ptr (cffi:null-pointer) 10))

(defun strtoull (char-ptr)
  (libpq-ffi::c-strtoull char-ptr (cffi:null-pointer) 10))

(defun strtoll (char-ptr)
  (libpq-ffi::c-strtoll char-ptr (cffi:null-pointer) 10))

(defun convert-raw-field (char-ptr type &key length encoding)
  (unless (cffi:null-pointer-p char-ptr)
    (case type
      (:double (libpq-ffi::atof char-ptr))
      (:int (libpq-ffi::atol char-ptr))
      (:int32 (libpq-ffi::atoi char-ptr))
      (:uint32 (strtoul char-ptr))
      (:uint (strtoul char-ptr))
      (:int64 (strtoll char-ptr))
      (:uint64 (strtoull char-ptr))
	  (:string (cffi:convert-from-foreign char-ptr :string))
	  (t (cffi:convert-from-foreign char-ptr :string)))))

(defun pg-query (query &optional (pg-connection *pg-connection*))
  (let ((conn-ptr (conn-ptr pg-connection)))
    (cffi:with-foreign-string (foreign-query query)
      (let ((result (libpq-ffi:PQexec conn-ptr foreign-query)))
        (when (cffi:null-pointer-p result)
          (error (libpq-ffi:PQerrorMessage conn-ptr)))
        (unwind-protect
			 (case (libpq-ffi:PQresultStatus result)
			   ;; User gave a command rather than a query
			   (:pgres_command_ok
				nil)
			   (:pgres_empty_query
				nil)
			   (:pgres_tuples_ok
				(let* ((num-fields (libpq-ffi:PQnfields result))
					   (result-types (make-type-list-for-auto num-fields result)))
				  (loop for tuple-index from 0 below (libpq-ffi:PQntuples result)
					 collect
					   (loop for i from 0 below num-fields
						  for field-name in (result-field-names num-fields result)
						  collect
							(cons field-name
								  (if (zerop (libpq-ffi:PQgetisnull result tuple-index i))
									  (convert-raw-field
									   (libpq-ffi:PQgetvalue result tuple-index i)
									   (nth i result-types))
								 
									  nil))))))
			   (t
				(error (libpq-ffi:PQresultErrorMessage result))))
          (libpq-ffi:PQclear result))))))

(export 'pg-query)
