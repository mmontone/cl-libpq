(defpackage libpq-ffi
  (:use :cl))

(in-package :libpq-ffi)

(cffi:define-foreign-library libpq
  (:unix (:or "libpq.so"))
  (t (:default "libpq")))

(cffi:use-foreign-library libpq)

(cffi:defcenum pgsql-ftype
  (:bytea 17)
  (:int2 21)
  (:int4 23)
  (:int8 20)
  (:float4 700)
  (:float8 701))

(cffi:defcfun ("atoi" atoi) :int
  (str (:pointer :char)))

(cffi:defcfun ("strtoul" c-strtoul) :unsigned-long-long
  (str (:pointer :unsigned-char))
  (endptr (:pointer :unsigned-char))
  (radix :int))

#-windows
(cffi:defcfun ("strtoull" c-strtoull) :unsigned-long-long
  (str (:pointer :unsigned-char))
  (endptr (:pointer :unsigned-char))
  (radix :int))

#-windows
(cffi:defcfun ("strtoll" c-strtoll) :long-long
  (str (:pointer :unsigned-char))
  (endptr (:pointer :unsigned-char))
  (radix :int))

#+windows
(uffi:def-function ("_strtoui64" c-strtoull)
    ((str (* :unsigned-char))
     (endptr (* :unsigned-char))
     (radix :int))
  :returning :unsigned-long-long)

#+windows
(uffi:def-function ("_strtoi64" c-strtoll)
    ((str (* :unsigned-char))
     (endptr (* :unsigned-char))
     (radix :int))
  :returning :long-long)

(cffi:defcfun ("atol" atol) :long
  (str (:pointer :unsigned-char)))

(cffi:defcfun ("atof" atof) :double
  (str (:pointer :unsigned-char)))

(cl:defmacro defanonenum (cl:&body enums)
  "Converts anonymous enums to defconstants."
  `(cl:progn ,@(cl:loop for value in enums
                        for index = 0 then (cl:1+ index)
                        when (cl:listp value) do (cl:setf index (cl:second value)
                                                          value (cl:first value))
                        collect `(cl:defconstant ,value ,index))))

(cl:eval-when (:compile-toplevel :load-toplevel)
  (cl:unless (cl:fboundp 'swig-lispify)
    (cl:defun swig-lispify (name flag cl:&optional (package cl:*package*))
      (cl:labels ((helper (lst last rest cl:&aux (c (cl:car lst)))
                    (cl:cond
                      ((cl:null lst)
                       rest)
                      ((cl:upper-case-p c)
                       (helper (cl:cdr lst) 'upper
                               (cl:case last
                                 ((lower digit) (cl:list* c #\- rest))
                                 (cl:t (cl:cons c rest)))))
                      ((cl:lower-case-p c)
                       (helper (cl:cdr lst) 'lower (cl:cons (cl:char-upcase c) rest)))
                      ((cl:digit-char-p c)
                       (helper (cl:cdr lst) 'digit
                               (cl:case last
                                 ((upper lower) (cl:list* c #\- rest))
                                 (cl:t (cl:cons c rest)))))
                      ((cl:char-equal c #\_)
                       (helper (cl:cdr lst) '_ (cl:cons #\- rest)))
                      (cl:t
                       (cl:error "Invalid character: ~A" c)))))
        (cl:let ((fix (cl:case flag
                        ((constant enumvalue) "+")
                        (variable "*")
                        (cl:t ""))))
          (cl:intern
           (cl:concatenate
            'cl:string
            fix
            (cl:nreverse (helper (cl:concatenate 'cl:list name) cl:nil cl:nil))
            fix)
           package))))))

;;;SWIG wrapper code ends here


(cl:defconstant PG_COPYRES_ATTRS #x01)

(cl:export 'PG_COPYRES_ATTRS)

(cl:defconstant PG_COPYRES_TUPLES #x02)

(cl:export 'PG_COPYRES_TUPLES)

(cl:defconstant PG_COPYRES_EVENTS #x04)

(cl:export 'PG_COPYRES_EVENTS)

(cl:defconstant PG_COPYRES_NOTICEHOOKS #x08)

(cl:export 'PG_COPYRES_NOTICEHOOKS)

(cffi:defcenum ConnStatusType
  :CONNECTION_OK
  :CONNECTION_BAD
  :CONNECTION_STARTED
  :CONNECTION_MADE
  :CONNECTION_AWAITING_RESPONSE
  :CONNECTION_AUTH_OK
  :CONNECTION_SETENV
  :CONNECTION_SSL_STARTUP
  :CONNECTION_NEEDED)

(cl:export 'ConnStatusType)

(cffi:defcenum PostgresPollingStatusType
  (:PGRES_POLLING_FAILED #.0)
  :PGRES_POLLING_READING
  :PGRES_POLLING_WRITING
  :PGRES_POLLING_OK
  :PGRES_POLLING_ACTIVE)

(cl:export 'PostgresPollingStatusType)

(cffi:defcenum ExecStatusType
  (:PGRES_EMPTY_QUERY #.0)
  :PGRES_COMMAND_OK
  :PGRES_TUPLES_OK
  :PGRES_COPY_OUT
  :PGRES_COPY_IN
  :PGRES_BAD_RESPONSE
  :PGRES_NONFATAL_ERROR
  :PGRES_FATAL_ERROR
  :PGRES_COPY_BOTH
  :PGRES_SINGLE_TUPLE)

(cl:export 'ExecStatusType)

(cffi:defcenum PGTransactionStatusType
  :PQTRANS_IDLE
  :PQTRANS_ACTIVE
  :PQTRANS_INTRANS
  :PQTRANS_INERROR
  :PQTRANS_UNKNOWN)

(cl:export 'PGTransactionStatusType)

(cffi:defcenum PGVerbosity
  :PQERRORS_TERSE
  :PQERRORS_DEFAULT
  :PQERRORS_VERBOSE)

(cl:export 'PGVerbosity)

(cffi:defcenum PGPing
  :PQPING_OK
  :PQPING_REJECT
  :PQPING_NO_RESPONSE
  :PQPING_NO_ATTEMPT)

(cl:export 'PGPing)

(cffi:defcstruct PGnotify
  (relname :string)
  (be_pid :int)
  (extra :string)
  (next :pointer))

(cl:export 'PGnotify)

(cl:export 'relname)

(cl:export 'be_pid)

(cl:export 'extra)

(cl:export 'next)

(cffi:defcstruct PQprintOpt
  (header :char)
  (align :char)
  (standard :char)
  (html3 :char)
  (expanded :char)
  (pager :char)
  (fieldSep :string)
  (tableOpt :string)
  (caption :string)
  (fieldName :pointer))

(cl:export 'PQprintOpt)

(cl:export 'header)

(cl:export 'align)

(cl:export 'standard)

(cl:export 'html3)

(cl:export 'expanded)

(cl:export 'pager)

(cl:export 'fieldSep)

(cl:export 'tableOpt)

(cl:export 'caption)

(cl:export 'fieldName)

(cffi:defcstruct PQconninfoOption
  (keyword :string)
  (envvar :string)
  (compiled :string)
  (val :string)
  (label :string)
  (dispchar :string)
  (dispsize :int))

(cl:export 'PQconninfoOption)

(cl:export 'keyword)

(cl:export 'envvar)

(cl:export 'compiled)

(cl:export 'val)

(cl:export 'label)

(cl:export 'dispchar)

(cl:export 'dispsize)

(cffi:defcstruct PQArgBlock
  (len :int)
  (isint :int)
  (u :pointer))

(cl:export 'PQArgBlock)

(cl:export 'len)

(cl:export 'isint)

(cl:export 'u)

(cffi:defcunion PQArgBlock_u
  (ptr :pointer)
  (integer :int))

(cl:export 'PQArgBlock_u)

(cl:export 'ptr)

(cl:export 'integer)

(cffi:defcstruct PGresAttDesc
  (name :string)
  (tableid :pointer)
  (columnid :int)
  (format :int)
  (typid :pointer)
  (typlen :int)
  (atttypmod :int))

(cl:export 'PGresAttDesc)

(cl:export 'name)

(cl:export 'tableid)

(cl:export 'columnid)

(cl:export 'format)

(cl:export 'typid)

(cl:export 'typlen)

(cl:export 'atttypmod)

(cffi:defcfun ("PQconnectStart" PQconnectStart) :pointer
  (conninfo :string))

(cl:export 'PQconnectStart)

(cffi:defcfun ("PQconnectStartParams" PQconnectStartParams) :pointer
  (keywords :pointer)
  (values :pointer)
  (expand_dbname :int))

(cl:export 'PQconnectStartParams)

(cffi:defcfun ("PQconnectPoll" PQconnectPoll) PostgresPollingStatusType
  (conn :pointer))

(cl:export 'PQconnectPoll)

(cffi:defcfun ("PQconnectdb" PQconnectdb) :pointer
  (conninfo :string))

(cl:export 'PQconnectdb)

(cffi:defcfun ("PQconnectdbParams" PQconnectdbParams) :pointer
  (keywords :pointer)
  (values :pointer)
  (expand_dbname :int))

(cl:export 'PQconnectdbParams)

(cffi:defcfun ("PQsetdbLogin" PQsetdbLogin) :pointer
  (pghost :string)
  (pgport :string)
  (pgoptions :string)
  (pgtty :string)
  (dbName :string)
  (login :string)
  (pwd :string))

(cl:export 'PQsetdbLogin)

(cffi:defcfun ("PQfinish" PQfinish) :void
  (conn :pointer))

(cl:export 'PQfinish)

(cffi:defcfun ("PQconndefaults" PQconndefaults) :pointer)

(cl:export 'PQconndefaults)

(cffi:defcfun ("PQconninfoParse" PQconninfoParse) :pointer
  (conninfo :string)
  (errmsg :pointer))

(cl:export 'PQconninfoParse)

(cffi:defcfun ("PQconninfo" PQconninfo) :pointer
  (conn :pointer))

(cl:export 'PQconninfo)

(cffi:defcfun ("PQconninfoFree" PQconninfoFree) :void
  (connOptions :pointer))

(cl:export 'PQconninfoFree)

(cffi:defcfun ("PQresetStart" PQresetStart) :int
  (conn :pointer))

(cl:export 'PQresetStart)

(cffi:defcfun ("PQresetPoll" PQresetPoll) PostgresPollingStatusType
  (conn :pointer))

(cl:export 'PQresetPoll)

(cffi:defcfun ("PQreset" PQreset) :void
  (conn :pointer))

(cl:export 'PQreset)

(cffi:defcfun ("PQgetCancel" PQgetCancel) :pointer
  (conn :pointer))

(cl:export 'PQgetCancel)

(cffi:defcfun ("PQfreeCancel" PQfreeCancel) :void
  (cancel :pointer))

(cl:export 'PQfreeCancel)

(cffi:defcfun ("PQcancel" PQcancel) :int
  (cancel :pointer)
  (errbuf :string)
  (errbufsize :int))

(cl:export 'PQcancel)

(cffi:defcfun ("PQrequestCancel" PQrequestCancel) :int
  (conn :pointer))

(cl:export 'PQrequestCancel)

(cffi:defcfun ("PQdb" PQdb) :string
  (conn :pointer))

(cl:export 'PQdb)

(cffi:defcfun ("PQuser" PQuser) :string
  (conn :pointer))

(cl:export 'PQuser)

(cffi:defcfun ("PQpass" PQpass) :string
  (conn :pointer))

(cl:export 'PQpass)

(cffi:defcfun ("PQhost" PQhost) :string
  (conn :pointer))

(cl:export 'PQhost)

(cffi:defcfun ("PQport" PQport) :string
  (conn :pointer))

(cl:export 'PQport)

(cffi:defcfun ("PQtty" PQtty) :string
  (conn :pointer))

(cl:export 'PQtty)

(cffi:defcfun ("PQoptions" PQoptions) :string
  (conn :pointer))

(cl:export 'PQoptions)

(cffi:defcfun ("PQstatus" PQstatus) ConnStatusType
  (conn :pointer))

(cl:export 'PQstatus)

(cffi:defcfun ("PQtransactionStatus" PQtransactionStatus) PGTransactionStatusType
  (conn :pointer))

(cl:export 'PQtransactionStatus)

(cffi:defcfun ("PQparameterStatus" PQparameterStatus) :string
  (conn :pointer)
  (paramName :string))

(cl:export 'PQparameterStatus)

(cffi:defcfun ("PQprotocolVersion" PQprotocolVersion) :int
  (conn :pointer))

(cl:export 'PQprotocolVersion)

(cffi:defcfun ("PQserverVersion" PQserverVersion) :int
  (conn :pointer))

(cl:export 'PQserverVersion)

(cffi:defcfun ("PQerrorMessage" PQerrorMessage) :string
  (conn :pointer))

(cl:export 'PQerrorMessage)

(cffi:defcfun ("PQsocket" PQsocket) :int
  (conn :pointer))

(cl:export 'PQsocket)

(cffi:defcfun ("PQbackendPID" PQbackendPID) :int
  (conn :pointer))

(cl:export 'PQbackendPID)

(cffi:defcfun ("PQconnectionNeedsPassword" PQconnectionNeedsPassword) :int
  (conn :pointer))

(cl:export 'PQconnectionNeedsPassword)

(cffi:defcfun ("PQconnectionUsedPassword" PQconnectionUsedPassword) :int
  (conn :pointer))

(cl:export 'PQconnectionUsedPassword)

(cffi:defcfun ("PQclientEncoding" PQclientEncoding) :int
  (conn :pointer))

(cl:export 'PQclientEncoding)

(cffi:defcfun ("PQsetClientEncoding" PQsetClientEncoding) :int
  (conn :pointer)
  (encoding :string))

(cl:export 'PQsetClientEncoding)

(cffi:defcfun ("PQgetssl" PQgetssl) :pointer
  (conn :pointer))

(cl:export 'PQgetssl)

(cffi:defcfun ("PQinitSSL" PQinitSSL) :void
  (do_init :int))

(cl:export 'PQinitSSL)

(cffi:defcfun ("PQinitOpenSSL" PQinitOpenSSL) :void
  (do_ssl :int)
  (do_crypto :int))

(cl:export 'PQinitOpenSSL)

(cffi:defcfun ("PQsetErrorVerbosity" PQsetErrorVerbosity) PGVerbosity
  (conn :pointer)
  (verbosity PGVerbosity))

(cl:export 'PQsetErrorVerbosity)

(cffi:defcfun ("PQtrace" PQtrace) :void
  (conn :pointer)
  (debug_port :pointer))

(cl:export 'PQtrace)

(cffi:defcfun ("PQuntrace" PQuntrace) :void
  (conn :pointer))

(cl:export 'PQuntrace)

(cffi:defcfun ("PQsetNoticeReceiver" PQsetNoticeReceiver) :pointer
  (conn :pointer)
  (proc :pointer)
  (arg :pointer))

(cl:export 'PQsetNoticeReceiver)

(cffi:defcfun ("PQsetNoticeProcessor" PQsetNoticeProcessor) :pointer
  (conn :pointer)
  (proc :pointer)
  (arg :pointer))

(cl:export 'PQsetNoticeProcessor)

(cffi:defcfun ("PQregisterThreadLock" PQregisterThreadLock) :pointer
  (newhandler :pointer))

(cl:export 'PQregisterThreadLock)

(cffi:defcfun ("PQexec" PQexec) :pointer
  (conn :pointer)
  (query :string))

(cl:export 'PQexec)

(cffi:defcfun ("PQexecParams" PQexecParams) :pointer
  (conn :pointer)
  (command :string)
  (nParams :int)
  (paramTypes :pointer)
  (paramValues :pointer)
  (paramLengths :pointer)
  (paramFormats :pointer)
  (resultFormat :int))

(cl:export 'PQexecParams)

(cffi:defcfun ("PQprepare" PQprepare) :pointer
  (conn :pointer)
  (stmtName :string)
  (query :string)
  (nParams :int)
  (paramTypes :pointer))

(cl:export 'PQprepare)

(cffi:defcfun ("PQexecPrepared" PQexecPrepared) :pointer
  (conn :pointer)
  (stmtName :string)
  (nParams :int)
  (paramValues :pointer)
  (paramLengths :pointer)
  (paramFormats :pointer)
  (resultFormat :int))

(cl:export 'PQexecPrepared)

(cffi:defcfun ("PQsendQuery" PQsendQuery) :int
  (conn :pointer)
  (query :string))

(cl:export 'PQsendQuery)

(cffi:defcfun ("PQsendQueryParams" PQsendQueryParams) :int
  (conn :pointer)
  (command :string)
  (nParams :int)
  (paramTypes :pointer)
  (paramValues :pointer)
  (paramLengths :pointer)
  (paramFormats :pointer)
  (resultFormat :int))

(cl:export 'PQsendQueryParams)

(cffi:defcfun ("PQsendPrepare" PQsendPrepare) :int
  (conn :pointer)
  (stmtName :string)
  (query :string)
  (nParams :int)
  (paramTypes :pointer))

(cl:export 'PQsendPrepare)

(cffi:defcfun ("PQsendQueryPrepared" PQsendQueryPrepared) :int
  (conn :pointer)
  (stmtName :string)
  (nParams :int)
  (paramValues :pointer)
  (paramLengths :pointer)
  (paramFormats :pointer)
  (resultFormat :int))

(cl:export 'PQsendQueryPrepared)

(cffi:defcfun ("PQsetSingleRowMode" PQsetSingleRowMode) :int
  (conn :pointer))

(cl:export 'PQsetSingleRowMode)

(cffi:defcfun ("PQgetResult" PQgetResult) :pointer
  (conn :pointer))

(cl:export 'PQgetResult)

(cffi:defcfun ("PQisBusy" PQisBusy) :int
  (conn :pointer))

(cl:export 'PQisBusy)

(cffi:defcfun ("PQconsumeInput" PQconsumeInput) :int
  (conn :pointer))

(cl:export 'PQconsumeInput)

(cffi:defcfun ("PQnotifies" PQnotifies) :pointer
  (conn :pointer))

(cl:export 'PQnotifies)

(cffi:defcfun ("PQputCopyData" PQputCopyData) :int
  (conn :pointer)
  (buffer :string)
  (nbytes :int))

(cl:export 'PQputCopyData)

(cffi:defcfun ("PQputCopyEnd" PQputCopyEnd) :int
  (conn :pointer)
  (errormsg :string))

(cl:export 'PQputCopyEnd)

(cffi:defcfun ("PQgetCopyData" PQgetCopyData) :int
  (conn :pointer)
  (buffer :pointer)
  (async :int))

(cl:export 'PQgetCopyData)

(cffi:defcfun ("PQgetline" PQgetline) :int
  (conn :pointer)
  (string :string)
  (length :int))

(cl:export 'PQgetline)

(cffi:defcfun ("PQputline" PQputline) :int
  (conn :pointer)
  (string :string))

(cl:export 'PQputline)

(cffi:defcfun ("PQgetlineAsync" PQgetlineAsync) :int
  (conn :pointer)
  (buffer :string)
  (bufsize :int))

(cl:export 'PQgetlineAsync)

(cffi:defcfun ("PQputnbytes" PQputnbytes) :int
  (conn :pointer)
  (buffer :string)
  (nbytes :int))

(cl:export 'PQputnbytes)

(cffi:defcfun ("PQendcopy" PQendcopy) :int
  (conn :pointer))

(cl:export 'PQendcopy)

(cffi:defcfun ("PQsetnonblocking" PQsetnonblocking) :int
  (conn :pointer)
  (arg :int))

(cl:export 'PQsetnonblocking)

(cffi:defcfun ("PQisnonblocking" PQisnonblocking) :int
  (conn :pointer))

(cl:export 'PQisnonblocking)

(cffi:defcfun ("PQisthreadsafe" PQisthreadsafe) :int)

(cl:export 'PQisthreadsafe)

(cffi:defcfun ("PQping" PQping) PGPing
  (conninfo :string))

(cl:export 'PQping)

(cffi:defcfun ("PQpingParams" PQpingParams) PGPing
  (keywords :pointer)
  (values :pointer)
  (expand_dbname :int))

(cl:export 'PQpingParams)

(cffi:defcfun ("PQflush" PQflush) :int
  (conn :pointer))

(cl:export 'PQflush)

(cffi:defcfun ("PQfn" PQfn) :pointer
  (conn :pointer)
  (fnid :int)
  (result_buf :pointer)
  (result_len :pointer)
  (result_is_int :int)
  (args :pointer)
  (nargs :int))

(cl:export 'PQfn)

(cffi:defcfun ("PQresultStatus" PQresultStatus) ExecStatusType
  (res :pointer))

(cl:export 'PQresultStatus)

(cffi:defcfun ("PQresStatus" PQresStatus) :string
  (status ExecStatusType))

(cl:export 'PQresStatus)

(cffi:defcfun ("PQresultErrorMessage" PQresultErrorMessage) :string
  (res :pointer))

(cl:export 'PQresultErrorMessage)

(cffi:defcfun ("PQresultErrorField" PQresultErrorField) :string
  (res :pointer)
  (fieldcode :int))

(cl:export 'PQresultErrorField)

(cffi:defcfun ("PQntuples" PQntuples) :int
  (res :pointer))

(cl:export 'PQntuples)

(cffi:defcfun ("PQnfields" PQnfields) :int
  (res :pointer))

(cl:export 'PQnfields)

(cffi:defcfun ("PQbinaryTuples" PQbinaryTuples) :int
  (res :pointer))

(cl:export 'PQbinaryTuples)

(cffi:defcfun ("PQfname" PQfname) :string
  (res :pointer)
  (field_num :int))

(cl:export 'PQfname)

(cffi:defcfun ("PQfnumber" PQfnumber) :int
  (res :pointer)
  (field_name :string))

(cl:export 'PQfnumber)

(cffi:defcfun ("PQftable" PQftable) :pointer
  (res :pointer)
  (field_num :int))

(cl:export 'PQftable)

(cffi:defcfun ("PQftablecol" PQftablecol) :int
  (res :pointer)
  (field_num :int))

(cl:export 'PQftablecol)

(cffi:defcfun ("PQfformat" PQfformat) :int
  (res :pointer)
  (field_num :int))

(cl:export 'PQfformat)

(cffi:defcfun ("PQftype" PQftype) :int
  (res :pointer)
  (field_num :int))

(cl:export 'PQftype)

(cffi:defcfun ("PQfsize" PQfsize) :int
  (res :pointer)
  (field_num :int))

(cl:export 'PQfsize)

(cffi:defcfun ("PQfmod" PQfmod) :int
  (res :pointer)
  (field_num :int))

(cl:export 'PQfmod)

(cffi:defcfun ("PQcmdStatus" PQcmdStatus) :string
  (res :pointer))

(cl:export 'PQcmdStatus)

(cffi:defcfun ("PQoidStatus" PQoidStatus) :string
  (res :pointer))

(cl:export 'PQoidStatus)

(cffi:defcfun ("PQoidValue" PQoidValue) :pointer
  (res :pointer))

(cl:export 'PQoidValue)

(cffi:defcfun ("PQcmdTuples" PQcmdTuples) :string
  (res :pointer))

(cl:export 'PQcmdTuples)

(cffi:defcfun ("PQgetvalue" PQgetvalue) (:pointer :unsigned-char)
  (res :pointer)
  (tup_num :int)
  (field_num :int))

(cl:export 'PQgetvalue)

(cffi:defcfun ("PQgetlength" PQgetlength) :int
  (res :pointer)
  (tup_num :int)
  (field_num :int))

(cl:export 'PQgetlength)

(cffi:defcfun ("PQgetisnull" PQgetisnull) :int
  (res :pointer)
  (tup_num :int)
  (field_num :int))

(cl:export 'PQgetisnull)

(cffi:defcfun ("PQnparams" PQnparams) :int
  (res :pointer))

(cl:export 'PQnparams)

(cffi:defcfun ("PQparamtype" PQparamtype) :pointer
  (res :pointer)
  (param_num :int))

(cl:export 'PQparamtype)

(cffi:defcfun ("PQdescribePrepared" PQdescribePrepared) :pointer
  (conn :pointer)
  (stmt :string))

(cl:export 'PQdescribePrepared)

(cffi:defcfun ("PQdescribePortal" PQdescribePortal) :pointer
  (conn :pointer)
  (portal :string))

(cl:export 'PQdescribePortal)

(cffi:defcfun ("PQsendDescribePrepared" PQsendDescribePrepared) :int
  (conn :pointer)
  (stmt :string))

(cl:export 'PQsendDescribePrepared)

(cffi:defcfun ("PQsendDescribePortal" PQsendDescribePortal) :int
  (conn :pointer)
  (portal :string))

(cl:export 'PQsendDescribePortal)

(cffi:defcfun ("PQclear" PQclear) :void
  (res :pointer))

(cl:export 'PQclear)

(cffi:defcfun ("PQfreemem" PQfreemem) :void
  (ptr :pointer))

(cl:export 'PQfreemem)

(cl:defconstant PQnoPasswordSupplied "fe_sendauth: no password supplied\n")

(cl:export 'PQnoPasswordSupplied)

(cffi:defcfun ("PQmakeEmptyPGresult" PQmakeEmptyPGresult) :pointer
  (conn :pointer)
  (status ExecStatusType))

(cl:export 'PQmakeEmptyPGresult)

(cffi:defcfun ("PQcopyResult" PQcopyResult) :pointer
  (src :pointer)
  (flags :int))

(cl:export 'PQcopyResult)

(cffi:defcfun ("PQsetResultAttrs" PQsetResultAttrs) :int
  (res :pointer)
  (numAttributes :int)
  (attDescs :pointer))

(cl:export 'PQsetResultAttrs)

(cffi:defcfun ("PQresultAlloc" PQresultAlloc) :pointer
  (res :pointer)
  (nBytes :pointer))

(cl:export 'PQresultAlloc)

(cffi:defcfun ("PQsetvalue" PQsetvalue) :int
  (res :pointer)
  (tup_num :int)
  (field_num :int)
  (value :string)
  (len :int))

(cl:export 'PQsetvalue)

(cffi:defcfun ("PQescapeStringConn" PQescapeStringConn) :pointer
  (conn :pointer)
  (to :string)
  (from :string)
  (length :pointer)
  (error :pointer))

(cl:export 'PQescapeStringConn)

(cffi:defcfun ("PQescapeLiteral" PQescapeLiteral) :string
  (conn :pointer)
  (str :string)
  (len :pointer))

(cl:export 'PQescapeLiteral)

(cffi:defcfun ("PQescapeIdentifier" PQescapeIdentifier) :string
  (conn :pointer)
  (str :string)
  (len :pointer))

(cl:export 'PQescapeIdentifier)

(cffi:defcfun ("PQescapeByteaConn" PQescapeByteaConn) :pointer
  (conn :pointer)
  (from :pointer)
  (from_length :pointer)
  (to_length :pointer))

(cl:export 'PQescapeByteaConn)

(cffi:defcfun ("PQunescapeBytea" PQunescapeBytea) :pointer
  (strtext :pointer)
  (retbuflen :pointer))

(cl:export 'PQunescapeBytea)

(cffi:defcfun ("PQescapeString" PQescapeString) :pointer
  (to :string)
  (from :string)
  (length :pointer))

(cl:export 'PQescapeString)

(cffi:defcfun ("PQescapeBytea" PQescapeBytea) :pointer
  (from :pointer)
  (from_length :pointer)
  (to_length :pointer))

(cl:export 'PQescapeBytea)

(cffi:defcfun ("PQprint" PQprint) :void
  (fout :pointer)
  (res :pointer)
  (ps :pointer))

(cl:export 'PQprint)

(cffi:defcfun ("PQdisplayTuples" PQdisplayTuples) :void
  (res :pointer)
  (fp :pointer)
  (fillAlign :int)
  (fieldSep :string)
  (printHeader :int)
  (quiet :int))

(cl:export 'PQdisplayTuples)

(cffi:defcfun ("PQprintTuples" PQprintTuples) :void
  (res :pointer)
  (fout :pointer)
  (printAttName :int)
  (terseOutput :int)
  (width :int))

(cl:export 'PQprintTuples)

(cffi:defcfun ("lo_open" lo_open) :int
  (conn :pointer)
  (lobjId :pointer)
  (mode :int))

(cl:export 'lo_open)

(cffi:defcfun ("lo_close" lo_close) :int
  (conn :pointer)
  (fd :int))

(cl:export 'lo_close)

(cffi:defcfun ("lo_read" lo_read) :int
  (conn :pointer)
  (fd :int)
  (buf :string)
  (len :pointer))

(cl:export 'lo_read)

(cffi:defcfun ("lo_write" lo_write) :int
  (conn :pointer)
  (fd :int)
  (buf :string)
  (len :pointer))

(cl:export 'lo_write)

(cffi:defcfun ("lo_lseek" lo_lseek) :int
  (conn :pointer)
  (fd :int)
  (offset :int)
  (whence :int))

(cl:export 'lo_lseek)

(cffi:defcfun ("lo_lseek64" lo_lseek64) :pointer
  (conn :pointer)
  (fd :int)
  (offset :pointer)
  (whence :int))

(cl:export 'lo_lseek64)

(cffi:defcfun ("lo_creat" lo_creat) :pointer
  (conn :pointer)
  (mode :int))

(cl:export 'lo_creat)

(cffi:defcfun ("lo_create" lo_create) :pointer
  (conn :pointer)
  (lobjId :pointer))

(cl:export 'lo_create)

(cffi:defcfun ("lo_tell" lo_tell) :int
  (conn :pointer)
  (fd :int))

(cl:export 'lo_tell)

(cffi:defcfun ("lo_tell64" lo_tell64) :pointer
  (conn :pointer)
  (fd :int))

(cl:export 'lo_tell64)

(cffi:defcfun ("lo_truncate" lo_truncate) :int
  (conn :pointer)
  (fd :int)
  (len :pointer))

(cl:export 'lo_truncate)

(cffi:defcfun ("lo_truncate64" lo_truncate64) :int
  (conn :pointer)
  (fd :int)
  (len :pointer))

(cl:export 'lo_truncate64)

(cffi:defcfun ("lo_unlink" lo_unlink) :int
  (conn :pointer)
  (lobjId :pointer))

(cl:export 'lo_unlink)

(cffi:defcfun ("lo_import" lo_import) :pointer
  (conn :pointer)
  (filename :string))

(cl:export 'lo_import)

(cffi:defcfun ("lo_import_with_oid" lo_import_with_oid) :pointer
  (conn :pointer)
  (filename :string)
  (lobjId :pointer))

(cl:export 'lo_import_with_oid)

(cffi:defcfun ("lo_export" lo_export) :int
  (conn :pointer)
  (lobjId :pointer)
  (filename :string))

(cl:export 'lo_export)

(cffi:defcfun ("PQlibVersion" PQlibVersion) :int)

(cl:export 'PQlibVersion)

(cffi:defcfun ("PQmblen" PQmblen) :int
  (s :string)
  (encoding :int))

(cl:export 'PQmblen)

(cffi:defcfun ("PQdsplen" PQdsplen) :int
  (s :string)
  (encoding :int))

(cl:export 'PQdsplen)

(cffi:defcfun ("PQenv2encoding" PQenv2encoding) :int)

(cl:export 'PQenv2encoding)

(cffi:defcfun ("PQencryptPassword" PQencryptPassword) :string
  (passwd :string)
  (user :string))

(cl:export 'PQencryptPassword)

(cffi:defcfun ("pg_char_to_encoding" pg_char_to_encoding) :int
  (name :string))

(cl:export 'pg_char_to_encoding)

(cffi:defcfun ("pg_encoding_to_char" pg_encoding_to_char) :string
  (encoding :int))

(cl:export 'pg_encoding_to_char)

(cffi:defcfun ("pg_valid_server_encoding_id" pg_valid_server_encoding_id) :int
  (encoding :int))

(cl:export 'pg_valid_server_encoding_id)
