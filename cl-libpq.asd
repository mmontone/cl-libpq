(asdf:defsystem #:cl-libpq
  :description "Postgresql libpq library ffi bindings"
  :author "Mariano Montone <marianomontone@gmail.com>"
  :license "MIT"
  :serial t
  :components ((:file "package")
			   (:file "ffi")
               (:file "cl-libpq"))
  :depends-on (:cffi :bordeaux-threads))
